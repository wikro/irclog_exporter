package main

import (
  "fmt"
  "os"
//  "io"
//  "io/ioutil"
  "bufio"
  "net/http"
  "time"
  "log"
  "strings"
//  "encoding/json"
//  "path/filepath"
)

type Metrics struct {
  NumberLines int
  NumberDucks int
}

type LogLine struct {
  Time string
  Nick string
  Message string
}

type IrcLog struct {
  FilePath string
  LogMetrics Metrics
}

//var ircLogs []IrcLog
var metrics Metrics

//func readSettings() ([]byte){
//  dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
//  if err != nil {
//    log.Fatal(err)
//  }
//
//  data, err := ioutil.ReadFile(filepath.Join(dir, "settings.json"))
//  if err != nil {
//    log.Fatal(err)
//  }
//
//  return data
//}

func parseLog() {
  file, err := os.Open("/home/robert/.weechat/logs/irc/snoonet.#offtopic.log")
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()

  var l LogLine
  var s []string
  m := Metrics { NumberLines: 0, NumberDucks: 0  }

  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    l.Time, l.Nick, l.Message = "", "", "" // reset LogLine
    s = strings.Split(scanner.Text(), "\t") // scanner.Text() = full line

    if len(s) > 0 {
      l.Time = s[0]
    }

    if len(s) > 1 {
      l.Nick = s[1]
    }

    if len(s) > 2 {
      l.Message = s[2]
    }

    if strings.Contains(l.Nick, "gonzobot") && strings.Contains(l.Message, "\u30fb") {
      m.NumberDucks++
    }

    m.NumberLines++
  }
  if err = scanner.Err(); err != nil {
    log.Fatal(err)
  } else {
    metrics = m
  }
}

func setInterval(f func(), s int) {
  go func() {
    var interval time.Duration
    var next time.Time
    for {
      f()

      interval = time.Duration(s) * time.Second
      next = time.Now().Add(interval).Truncate(interval)

      time.Sleep(time.Until(next))
    }
  }()
}

func handler(w http.ResponseWriter, r *http.Request) {
  fmt.Fprintf(w, "# TYPE irc_log_total counter\n")
  fmt.Fprintf(w, "irc_log_total{log=\"%s\",number=\"lines\"} %d\n", "snoonet.#offtopic.log", metrics.NumberLines)
  fmt.Fprintf(w, "irc_log_total{log=\"%s\",number=\"ducks\"} %d\n", "snoonet.#offtopic.log", metrics.NumberDucks)
}

func main() {
//  readSettings()

  setInterval(parseLog, 60)

  http.HandleFunc("/metrics", handler)
  log.Fatal(http.ListenAndServe(":9226",  nil))
}
